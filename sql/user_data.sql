--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Ubuntu 14.1-1.pgdg21.10+1)
-- Dumped by pg_dump version 14.1 (Ubuntu 14.1-1.pgdg21.10+1)

-- Started on 2022-01-01 18:25:04 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE php_crud;
--
-- TOC entry 3350 (class 1262 OID 16386)
-- Name: php_crud; Type: DATABASE; Schema: -; Owner: biswas
--

CREATE DATABASE php_crud WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_IN';


ALTER DATABASE php_crud OWNER TO biswas;

\connect php_crud

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3344 (class 0 OID 16405)
-- Dependencies: 210
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: biswas
--

INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (1, 'Bari Guiraud', 'bguiraud0@senate.gov', '693-215-9346', 'Female', '1368 Pennsylvania Court', 'Minas de Marcona', NULL, 'Peru', 'China', '2021-04-05 00:00:00+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (2, 'Boonie Trymme', 'btrymme1@hud.gov', '258-879-3902', 'Female', '51 Loomis Plaza', 'Choszczno', '73-201', 'Poland', 'Uruguay', '2021-06-06 00:00:00+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (3, 'Janeczka Bushe', 'jbushe2@answers.com', '790-953-2138', 'Female', '59 Claremont Trail', 'Melchor de Mencos', '17011', 'Guatemala', 'Nigeria', '2021-05-12 00:00:00+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (4, 'Porter Pavlenkov', 'ppavlenkov7@ycombinator.com', '248-429-8190', 'Female', '89 Westport Pass', 'Fontanka', NULL, 'Ukraine', 'Ghana', '2021-12-07 00:00:00+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (5, 'Carolann Snashall', 'csnashall8@yolasite.com', '835-106-2823', 'Male', '28 Westridge Park', 'Ilha Solteira', '15385-000', 'Brazil', 'Guinea', '2021-03-03 00:00:00+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (6, 'Monika Tomasoni', 'mtomasonid@reddit.com', '250-605-7233', 'Male', '289 Elgar Junction', 'Tiantang', NULL, 'China', 'France', '2021-06-09 00:00:00+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (7, 'Hamid Parradye', 'hparradyef@slate.com', '558-616-5217', 'Male', '874 Valley Edge Road', 'Dalkey', 'A96', 'Ireland', 'Philippines', '2021-05-04 00:00:00+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (8, 'Lulita Fenny', 'lfennyj@wunderground.com', '619-829-3753', 'Male', '33 Columbus Lane', 'Duanshen', NULL, 'China', 'Chile', '2021-04-08 00:00:00+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (9, 'Dolph Keuneke', 'dkeuneke0@wikia.com', '193-353-3558', 'Female', '67285 Acker Junction', 'Srostki', '659375', 'Russia', 'Indonesia', '2021-12-21 21:03:17+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (10, 'Morten Order', 'morder1@devhub.com', '977-945-8038', 'Female', '6 Nobel Court', 'Tahala', NULL, 'Morocco', 'Honduras', '2021-10-15 11:17:17+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (11, 'Carolyne Croasdale', 'ccroasdale2@typepad.com', '499-949-9383', 'Male', '3 Straubel Center', 'Tanshan', NULL, 'China', 'China', '2021-07-20 20:03:02+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (12, 'Cathrin Bentz', 'cbentz3@sciencedirect.com', '173-210-7140', 'Male', '13 Eastlawn Alley', 'Tayang', NULL, 'China', 'Brazil', '2021-07-17 00:18:22+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (13, 'Stanislas Pettigrew', 'spettigrew4@netlog.com', '925-838-1497', 'Male', '669 Bayside Road', 'Chaman', '86000', 'Pakistan', 'Vietnam', '2021-10-23 19:48:22+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (14, 'Diego Louden', 'dlouden5@jalbum.net', '516-241-8821', 'Male', '914 Ryan Court', 'Sailaitang', NULL, 'China', 'Brazil', '2021-09-11 15:15:47+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (15, 'Gerick Linne', 'glinne6@washington.edu', '855-709-7708', 'Male', '1474 Superior Lane', 'Lianhe', NULL, 'China', 'China', '2021-12-08 06:21:45+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (16, 'Pavla Brammar', 'pbrammar7@example.com', '932-494-1553', 'Female', '65 Ridgeway Alley', 'Xijiao', NULL, 'China', 'Indonesia', '2021-06-29 01:59:58+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (17, 'Finn Ranscombe', 'franscombe8@symantec.com', '448-136-1579', 'Female', '34442 Brentwood Street', 'Cikole', NULL, 'Indonesia', 'Serbia', '2021-09-07 14:34:01+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (18, 'Christalle Dibnah', 'cdibnah9@abc.net.au', '762-125-6308', 'Male', '65023 Rieder Street', 'Camagüey', NULL, 'Cuba', 'Philippines', '2021-10-01 10:45:57+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (19, 'Karlie Beynke', 'kbeynkea@google.it', '823-217-0782', 'Male', '9 Parkside Hill', 'Ribamar', '2530-631', 'Portugal', 'China', '2021-04-02 16:28:28+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (20, 'Moe Toulson', 'mtoulsonb@patch.com', '141-145-9956', 'Female', '0 Thierer Street', 'Sāqayn', NULL, 'Yemen', 'Brazil', '2021-12-07 16:49:57+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (21, 'Hakim Roadknight', 'hroadknightc@sciencedaily.com', '162-789-7393', 'Non-binary', '5 Hooker Alley', 'Fuying', NULL, 'China', 'Ukraine', '2021-01-12 04:22:53+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (22, 'Hardy Halvorsen', 'hhalvorsend@feedburner.com', '239-500-9949', 'Male', '83 Anzinger Junction', 'Capanema', '85760-000', 'Brazil', 'Morocco', '2021-11-17 07:04:00+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (23, 'Fredric Beckley', 'fbeckleyf@smh.com.au', '153-802-7815', 'Male', '5 Emmet Court', 'Hässelby', '165 60', 'Sweden', 'Sweden', '2021-10-20 18:42:11+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (24, 'Bathsheba Cunradi', 'bcunradig@usnews.com', '157-621-0051', 'Male', '858 Muir Center', 'Saint-Brieuc', '22042 CEDEX 2', 'France', 'Afghanistan', '2021-09-15 06:24:00+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (25, 'Delmore Issacoff', 'dissacoffh@ucsd.edu', '360-562-7143', 'Male', '1 Towne Avenue', 'Sāqayn', NULL, 'Yemen', 'Pakistan', '2021-09-14 10:34:50+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (26, 'Alphard Bravey', 'abraveyi@illinois.edu', '691-279-4723', 'Male', '33 Kim Parkway', 'Betzdorf', 'L-6832', 'Luxembourg', 'China', '2021-01-28 23:22:08+05:30');
INSERT INTO public.users (id, full_name, email_id, phone_no, gender, address, city, zip_code, country, region, created_at) VALUES (27, 'Pancho Lissimore', 'plissimorej@nature.com', '883-208-8865', 'Male', '95675 Mifflin Lane', 'Łaszczów', '22-650', 'Poland', 'China', '2021-12-08 16:01:15+05:30');


--
-- TOC entry 3352 (class 0 OID 0)
-- Dependencies: 209
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: biswas
--

SELECT pg_catalog.setval('public.users_id_seq', 28, true);


-- Completed on 2022-01-01 18:25:05 IST

--
-- PostgreSQL database dump complete
--

