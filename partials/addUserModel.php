<div class="reveal" id="createUserModel" data-reveal>
	<form data-abide novalidate method="POST">
		<div class="cell small-10 medium-8 large-8">
			<!-- Full Name -->
			<label>Full Name
				<input type="text" placeholder="Jon Deo" aria-describedby="exampleFullName" id="full_name" required pattern="text"> <span class="form-error">
          Please Enter Full Name
        </span> </label>
			<p class="help-text" id="exampleFullName">Enter Full Name like John Deo</p>
		</div>
		<!-- Email Address -->
		<div class="cell small-10">
			<label>Email Address
				<input type="text" placeholder="deo.john@exmaple.com" aria-describedby="exampleEmailId" required pattern="email" id="email_id"> <span class="form-error">
          Please Enter Email Address
        </span> </label>
			<p class="help-text" id="exampleEmailId">Enter Full Email Address like deo.john@exmaple.com</p>
		</div>
		<!-- Phone Number -->
		<div class="cell small-10">
			<label>Phone Number
				<input type="text" placeholder="1234567890" aria-describedby="examplePhoneNo" required pattern="number" id="phone_no" minlength="8"> <span class="form-error">
          Please Enter Phone Number
        </span> </label>
			<p class="help-text" id="examplePhoneNo">Enter Full Phone Number like 1234567890</p>
		</div>
		<!-- Gender -->
		<div class="cell small-10">
			<label>Gender
				<select required id="gender">
					<option value="male">Male</option>
					<option value="female">Female</option>
					<option value="Other">Other</option>
					<option value="Not Telling">Not Telling</option>
				</select>
			</label>
		</div>
		<!-- Address -->
		<div class="cell small-10">
			<label>Street Address
				<input type="text" placeholder="1234, Street Name" aria-describedby="exampleAddress" required pattern="text" id="address"> <span class="form-error">
          Please Enter Street Address
        </span> </label>
			<p class="help-text" id="exampleAddress">Enter Your Address like 1234, Street Name</p>
		</div>
		<!-- City -->
		<div class="cell small-10">
    <label>City
      <input type="text" placeholder="City" aria-describedby="exampleCity" required pattern="text" id="city"> <span class="form-error">
        Please Enter City
      </span> </label>
    <p class="help-text" id="exampleCity">Enter Your City</p>
  </div>
		<!-- Zip Code -->
		<div class="cell small-10">
			<label>Zip Code/Postal Code
				<input type="text" placeholder="123456" aria-describedby="exampleCode" pattern="number" id="zip_code"> <span class="form-error">
          Please Postal Code
        </span> </label>
			<p class="help-text" id="exampleCode">Enter Postal Code like 123456</p>
		</div>
		<div class="cell small-10">
			<select name="country" id="country" class="country"></select>
		</div>
		<div class="cell small-10">
		<select name="region" id="region" >
  <option value="Africa">Africa</option>
  <option value="Asia">Asia</option>
  <option value="The Caribbean">The Caribbean</option>
  <option value="Central America">Central America</option>
  <option value="Europe">Europe</option>
  <option value="North America">North America</option>
  <option value="Oceania">Oceania</option>
  <option value="South America">South America</option>

  </select>
		</div>
		<div class="cell small-10">
			<button type="submit" class="button" id="submit">Submit</button>
		</div>
	</form>
	<button class="close-button" data-close aria-label="Close modal" type="button"> <span aria-hidden="true">&times;</span> </button>
</div>