<div class="grid-x grid-padding-x align-center get_start">
	<div class="cell small-8">
		<div class="card">
			<div class="card-divider">
				<h1 class="text-center">Welcome !</h1> </div>
			<div class="card-section">
				<h2>PHP Crud</h2>
				<p>You Can Create, Read, Update, Delete</p>
				<button class="button" id="get_start_btn">Get Started</button>
			</div>
		</div>
	</div>
</div>