<div class="grid-x grid-padding-x align-center hide_table index_table">
	<div class="cell small-10">
		<div class="card">
			<div class="card-divider">
				<h1>PHP Crud</h1> </div>
			<div class="card-section">
				<button class="button" data-open="createUserModel" id="add_user">Add User</button>
				<div class="table">
					<table>
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email Address</th>
								<th>Phone Number</th>
								<th>Gender</th>
								<th>Address</th>
								<th>City</th>
								<th>Zip Code</th>
								<th>Country</th>
								<th>region</th>
								<th>Register Date</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody id="data_show"> </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>