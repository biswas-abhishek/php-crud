<br>
<hr>
<div class="grid-x grid-padding-x align-center">
<div class="cell small-8">
<h4 class="subheader">Copyright By Abhishek Biswas</h4>
<h5 class="subheader">Source Code - <a href="https://gitlab.com/biswas-abhishek/php-crud" target="_blank" rel="noopener noreferrer" >Gitlab</a></h5>
</div>
</div>

<script src="js/vendor/jquery.min.js"></script>
<script src="js/vendor/what-input.min.js"></script>
<script src="js/vendor/foundation.min.js"></script>
<script src="js/app.js"></script>
</body>

</html>
