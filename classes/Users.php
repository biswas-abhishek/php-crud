<?php
class User
{
    //Connection
    private $connection;

    //Table Name
    private $db_table = "users";

    // Columns
    public $id;
    public $full_name;
    public $email_id;
    public $phone_no;
    public $gender;
    public $address;
    public $city;
    public $zip_code;
    public $country;
    public $region;
    public $created_at;

    //DB Connection
    public function __construct($db)
    {
        $this->connection = $db;
    }

    //Get All Data
    public function getUser()
    {
        $sql = "SELECT * FROM " . $this->db_table . "";
        $stmt = $this
            ->connection
            ->prepare($sql);
        $stmt->execute();
        return $stmt;
    }

    //Single Date
    public function getSingleUser()
    {
        $sql = "SELECT
                    *
                  FROM
                    " . $this->db_table . "
                    WHERE 
                   id = ?
                ";

        $stmt = $this
            ->connection
            ->prepare($sql);

        $stmt->bindValue(1, $this->id);

        $stmt->execute();

        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->id = $dataRow['id'];
        $this->full_name = $dataRow['full_name'];
        $this->email_id = $dataRow['email_id'];
        $this->phone_no = $dataRow['phone_no'];
        $this->gender = $dataRow['gender'];
        $this->address = $dataRow['address'];
        $this->city = $dataRow['city'];
        $this->zip_code = $dataRow['zip_code'];
        $this->country = $dataRow['country'];
        $this->region = $dataRow['region'];
        $this->created_at = $dataRow['created_at'];
    }

    //Create User
    public function createUser()
    {
        $sql = "INSERT INTO
                         " . $this->db_table . "(full_name,email_id,phone_no,gender,address,city,zip_code,country,region) 
                    VALUES

                    (:full_name, 
                     :email_id, 
                     :phone_no, 
                     :gender, 
                     :address,
                     :city, 
                     :zip_code, 
                     :country, 
                     :region
                     
    )";

        $stmt = $this
            ->connection
            ->prepare($sql);

        // sanitize
        $this->full_name = htmlspecialchars(strip_tags($this->full_name));
        $this->email_id = htmlspecialchars(strip_tags($this->email_id));
        $this->phone_no = htmlspecialchars(strip_tags($this->phone_no));
        $this->gender = htmlspecialchars(strip_tags($this->gender));
        $this->address = htmlspecialchars(strip_tags($this->address));
        $this->city = htmlspecialchars(strip_tags($this->city));
        $this->zip_code = htmlspecialchars(strip_tags($this->zip_code));
        $this->country = htmlspecialchars(strip_tags($this->country));
        $this->region = htmlspecialchars(strip_tags($this->region));

        // bind data
        $stmt->bindValue(":full_name", $this->full_name);
        $stmt->bindValue(":email_id", $this->email_id);
        $stmt->bindValue(":phone_no", $this->phone_no);
        $stmt->bindValue(":gender", $this->gender);
        $stmt->bindValue(":address", $this->address);
        $stmt->bindValue(":city", $this->city);
        $stmt->bindValue(":zip_code", $this->zip_code);
        $stmt->bindValue(":country", $this->country);
        $stmt->bindValue(":region", $this->region);

        if ($stmt->execute())
        {
            return true;
        }
        return false;
    }

    //Update User
    public function updateUser()
    {
        $sql = "UPDATE
                        " . $this->db_table . "
                    SET
                    full_name = :full_name, 
                    email_id = :email_id, 
                    phone_no = :phone_no, 
                    gender = :gender, 
                    address = :address,
                    city=:city,
                    zip_code = :zip_code, 
                    country = :country, 
                    region = :region
                    WHERE 
                        id = :id";

        $stmt = $this
            ->connection
            ->prepare($sql);

        // sanitize
        $this->full_name = htmlspecialchars(strip_tags($this->full_name));
        $this->email_id = htmlspecialchars(strip_tags($this->email_id));
        $this->phone_no = htmlspecialchars(strip_tags($this->phone_no));
        $this->gender = htmlspecialchars(strip_tags($this->gender));
        $this->address = htmlspecialchars(strip_tags($this->address));
        $this->city = htmlspecialchars(strip_tags($this->city));
        $this->zip_code = htmlspecialchars(strip_tags($this->zip_code));
        $this->country = htmlspecialchars(strip_tags($this->country));
        $this->region = htmlspecialchars(strip_tags($this->region));

        // bind data
        $stmt->bindValue(":id", $this->id);
        $stmt->bindValue(":full_name", $this->full_name);
        $stmt->bindValue(":email_id", $this->email_id);
        $stmt->bindValue(":phone_no", $this->phone_no);
        $stmt->bindValue(":gender", $this->gender);
        $stmt->bindValue(":address", $this->address);
        $stmt->bindValue(":city", $this->city);
        $stmt->bindValue(":zip_code", $this->zip_code);
        $stmt->bindValue(":country", $this->country);
        $stmt->bindValue(":region", $this->region);

        if ($stmt->execute())
        {
            return true;
        }
        return false;
    }

    // Delete User
    public function deleteUser()
    {
        $sql = "DELETE FROM " . $this->db_table . " WHERE id = ?";
        $stmt = $this
            ->connection
            ->prepare($sql);
        $stmt->bindValue(1, $this->id);
        if ($stmt->execute())
        {
            return true;
        }
        return false;
    }
}
?>
