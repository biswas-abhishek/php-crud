<?php
require "./partials/header.php";
?>

<input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>">
<div class="grid-x grid-padding-x">
    <div class="cell small-10">
        <div class="card">
            <div class="card-divider">
                <h1>
        User Infomation
    </h1>
            </div>
            <section id="user_info"> </section>
        </div>
        <a href="https://php-crud-v1.herokuapp.com" class="button">Back</a>
    </div>
</div>
<script src="./js/singleUser.js"></script>

<?php
require "./partials/footer.php";
?>
