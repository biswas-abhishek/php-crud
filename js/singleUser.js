const UserInfo = document.querySelector("#user_info");
const UserId = document.querySelector("#id").value;

const singleUserData = async () => {
  const response = await fetch(`./apis/singleUser.php?id=${UserId}`);
  const data = await response.json();
  let tempData = `
  <p>
  <br /> Name - ${data.full_name}
  <br />
  <hr /> Email Address - ${data.email_id}
  <br />
  <hr /> Phone Number - ${data.phone_no}
  <br />
  <hr /> Gender - ${data.gender}
  <br />
  <hr /> Address - ${data.address}
  <br />
  <hr /> City - ${data.City}
  <br />
  <hr /> Zip\Postal Code - ${data.zip_code}
  <br />
  <hr /> Country - ${data.country}
  <br />
  <hr /> Region - ${data.region}
  <br />
  <hr /> Register At - ${data.created_at}
  <br />
  <hr />
</p>
                `;
  UserInfo.innerHTML = tempData;
};
singleUserData();
