const UserInfo = document.querySelector("#user_info");
const UserId = document.querySelector("#id").value;

const singleUserData = async () => {
  const response = await fetch(`./apis/singleUser.php?id=${UserId}`);
  const data = await response.json();
  let tempData = `
  <form data-abide novalidate method="post" class="update">
  <label
    >Full Name
    <input
      type="text"
      placeholder="Jon Deo"
      aria-describedby="exampleFullName"
      id="full_name"
      required
      pattern="text"
      value="${data.full_name}"
    />
    <span class="form-error"> Please Enter Full Name </span>
  </label>
  <p class="help-text" id="exampleFullName">Enter Full Name like John Deo</p>

  <label
    >Email Address
    <input
      type="text"
      placeholder="deo.john@exmaple.com"
      aria-describedby="exampleEmailId"
      required
      pattern="email"
      id="email_id"
      value="${data.email_id}"
    />
    <span class="form-error"> Please Enter Email Address </span>
  </label>
  <p class="help-text" id="exampleEmailId">
    Enter Full Email Address like deo.john@exmaple.com
  </p>

  <label
    >Phone Number
    <input
      type="text"
      placeholder="1234567890"
      aria-describedby="examplePhoneNo"
      required
      pattern="number"
      value="${data.phone_no}"
      id="phone_no"
    />
    <span class="form-error"> Please Enter Phone Number </span>
  </label>
  <p class="help-text" id="examplePhoneNo">
    Enter Full Phone Number like 1234567890
  </p>

  <label
    >Gender
    <select required id="gender">
      <option value="male">Male</option>
      <option value="female">Female</option>
      <option value="Other">Other</option>
      <option value="Not Telling">Not Telling</option>
    </select>
  </label>

  <label
    >Street Address with City
    <input
      type="text"
      placeholder="1234, Street Name, City"
      aria-describedby="exampleAddress"
      required
      pattern="text"
      id="address"
      value="${data.address}"
    />
    <span class="form-error"> Please Enter Street Address </span>
  </label>
  <p class="help-text" id="exampleAddress">
    Enter Your Address With City 1234, Street Name
  </p>

  <label
    >City
    <input
      type="text"
      placeholder="City"
      aria-describedby="exampleCity"
      required
      pattern="text"
      id="city"
      value="${data.city}"
    />
    <span class="form-error"> Please Enter City </span>
  </label>
  <p class="help-text" id="exampleCity">Enter Your City</p>

  <label
    >Zip Code/Postal Code
    <input
      type="text"
      placeholder="123456"
      aria-describedby="exampleCode"
      required
      pattern="number"
      value="${data.zip_code}"
      id="zip_code"
    />
    <span class="form-error"> Please Postal Code </span>
  </label>
  <p class="help-text" id="exampleCode">Enter Postal Code like 123456</p>

  <select name="country" class="updateCountry" id="country"></select>
  <select name="region" id="region" >
  <option value="Africa">Africa</option>
  <option value="Asia">Asia</option>
  <option value="The Caribbean">The Caribbean</option>
  <option value="Central America">Central America</option>
  <option value="Europe">Europe</option>
  <option value="North America">North America</option>
  <option value="Oceania">Oceania</option>
  <option value="South America">South America</option>

  </select>
  <div class="cell">
    <button type="submit" class="button success" id="update">Update</button>
    <a href="index.php" class="button alert float-right">Back</a>
  </div>
</form>
                `;
  UserInfo.innerHTML = tempData;
};
singleUserData();

setTimeout(() => {
  const updateCountry = document.querySelector(".updateCountry");
  const update = document.querySelector("#update");
  update.addEventListener("click", (e) => {
    e.preventDefault();
    const fullName = document.querySelector("#full_name").value;
    const emailId = document.querySelector("#email_id").value;
    const phoneNo = document.querySelector("#phone_no").value;
    const gender = document.querySelector("#gender").value;
    const address = document.querySelector("#address").value;
    const city = document.querySelector("#city").value;
    const zip_code = document.querySelector("#zip_code").value;
    const country = document.querySelector("#country").value;
    const region = document.querySelector("#region").value;

    const postData = {
      full_name: fullName,
      email_id: emailId,
      phone_no: phoneNo,
      gender,
      address,
      city,
      zip_code,
      country,
      region,
    };

    fetch(`./apis/updateUser.php?id=${UserId}`, {
      method: "PUT",
      body: JSON.stringify(postData),
    }).then((response) => {
      if (response.ok) {
        response.json();
        document.querySelector(".callout.secondary.small").innerHTML =
          "Update successful";
        document
          .querySelector(".callout.secondary.small")
          .classList.remove("hide");
        document
          .querySelector(".callout.secondary.small")
          .classList.add("success");
      } else {
        document.querySelector(".callout.secondary.small").innerHTML =
          "Update unsuccessful";
        document
          .querySelector(".callout.secondary.small")
          .classList.remove("hide");
        document
          .querySelector(".callout.secondary.small")
          .classList.add("alert");
      }
    });
  });

  fetch(`https://restcountries.com/v3.1/all`)
    .then((response) => response.json())
    .then((data) => {
      if (data.length > 0) {
        let temp = "";
        // let tempRegion = "";
        data.forEach((allData) => {
          temp += `
      <option value="${allData.name.common}">${allData.name.common}</option>
      `;
        });
        updateCountry.innerHTML = temp;
      }
    });
}, 3000);
