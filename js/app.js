$(document).foundation();

const getStartBtn = document.querySelector("#get_start_btn");
const getStart = document.querySelector(".get_start");
const indexTable = document.querySelector(".index_table");
const UserTable = document.querySelector("#data_show");
const updateBtn = document.querySelector("#update");
const country = document.querySelector(".country");
const region = document.querySelector(".region");
const addUser = document.querySelector("#add_user");

getStartBtn.addEventListener("click", () => {
  if (
    getStart.classList.contains("hide_start") &&
    indexTable.classList.contains("index_table")
  ) {
    getStart.classList.remove("hide_start");
    setTimeout(() => {
      getStart.classList.remove("hide_start_visually");
    }, 40);
    indexTable.classList.add("hide_table");
    setTimeout(() => {
      indexTable.classList.add("hide_table_visually");
    }, 40);
  } else {
    getStart.classList.add("hide_start_visually");
    setTimeout(() => {
      getStart.classList.add("hide_start");
    }, 400);
    indexTable.classList.remove("hide_table");
    setTimeout(() => {
      indexTable.classList.remove("hide_table_visually");
    }, 400);
  }
});

fetch("./apis/readData.php").then((response) => {
  response.json().then((data) => {
    if (response.ok) {
      if (data.body.length > 0) {
        let tempData = "";
        data.body.forEach((allData) => {
          tempData += `
          <tr>
          <td class="uid">${allData.id}</td>
          <td>${allData.full_name}</td>
          <td>${allData.email_id}</td>
          <td>${allData.phone_no}</td>
          <td>${allData.gender}</td>
          <td>${allData.address}</td>
          <td>${allData.city}</td>
          <td>${allData.zip_code}</td>
          <td>${allData.country}</td>
          <td>${allData.region}</td>
          <td>${allData.created_at}</td>
          <td>
            <a href="./user.php?id=${allData.id}" class="button small info">Detail</a>
            <a class="delete_user button small alert" href="./delete.php?id=${allData.id}"
              >Delete</a
            >
            <a class="update_user button small success" href="./update.php?id=${allData.id}"
              >Update</a
            >
          </td>
        </tr>      
                      `;
        });
        UserTable.innerHTML = tempData;
      }
    } else {
      document.querySelector("table").innerHTML = "<h1>No User found</h1>";
    }
  });
});

const submit = document.querySelector("#submit");
submit.addEventListener("click", (e) => {
  e.preventDefault();
  const fullName = document.querySelector("#full_name").value;
  const emailId = document.querySelector("#email_id").value;
  const phoneNo = document.querySelector("#phone_no").value;
  const gender = document.querySelector("#gender").value;
  const address = document.querySelector("#address").value;
  const city = document.querySelector("#city").value;
  const zip_code = document.querySelector("#zip_code").value;
  const country = document.querySelector("#country").value;
  const postData = {
    full_name: fullName,
    email_id: emailId,
    phone_no: phoneNo,
    gender,
    address,
    city,
    zip_code,
    country,
    region,
  };
  fetch(`./apis/createUser.php`, {
    method: "POST",
    body: JSON.stringify(postData),
  }).then((response) => {
    response.json();
  });
});

// Show Country
addUser.addEventListener("click", () => {
  fetch(`https://restcountries.com/v3.1/all`)
    .then((response) => response.json())
    .then((data) => {
      if (data.length > 0) {
        let temp = "";
        let tempRegion = "";
        data.forEach((allData) => {
          temp += `
      <option value="${allData.name.common}">${allData.name.common}</option>
      `;
          country.innerHTML = temp;
        });
      }
    });
});
