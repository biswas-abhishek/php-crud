<?php
header('Access-Control-Allow-Origin:*');
header('Content-Type:application/json; charset=UTF-8');
header('Access-Control-Allow-Methods:POST');
header('Access-Control-Max-Age:3600');
header('Access-Control-Allow-Headers:Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

require_once "../config/Database.php";
require_once "../classes/Users.php";

$database = new DatabaseConnection();
$db = $database->getConnection();

$items = new User($db);

$items->id = isset($_GET['id']) ? $_GET['id'] : die();

$items->getSingleUser();

if ($items->id != null)
{
    //create Array
    $user_arr = array(
        "id" => $items->id,
        "full_name" => $items->full_name,
        "email_id" => $items->email_id,
        "phone_no" => $items->phone_no,
        "gender" => $items->gender,
        "address" => $items->address,
        "city" => $items->city,
        "zip_code" => $items->zip_code,
        "country" => $items->country,
        "region" => $items->region,
        "created_at" => $items->created_at
    );

    http_response_code(200);

    echo json_encode($user_arr);
}
else
{
    http_response_code(404);
    echo json_encode("User not found.");
}
?>
