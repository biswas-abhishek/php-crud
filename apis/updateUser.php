<?php
header('Access-Control-Allow-Origin:*');
header('Content-Type:application/json; charset=UTF-8');
header('Access-Control-Allow-Methods:POST');
header('Access-Control-Max-Age:3600');
header('Access-Control-Allow-Headers:Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

require_once "../config/Database.php";
require_once "../classes/Users.php";

$database = new DatabaseConnection();
$db = $database->getConnection();

$items = new User($db);

$data = json_decode(file_get_contents('php://input'));
$items->id = isset($_GET['id']) ? $_GET['id'] : die();

$items->full_name = $data->full_name;
$items->email_id = $data->email_id;
$items->phone_no = $data->phone_no;
$items->gender = $data->gender;
$items->address = $data->address;
$items->city = $data->city;
$items->zip_code = $data->zip_code;
$items->country = $data->country;
$items->region = $data->region;

if ($items->updateUser())
{
    echo json_encode('User Updated successfully.');
}
else
{
    echo json_encode('User could not be Updated.');
}
?>
