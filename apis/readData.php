<?php
header('Access-Control-Allow-Origin:*');
header('Content-type:application/json;charset=UTF-8');

require_once "../config/Database.php";
require_once "../classes/Users.php";

$database = new DatabaseConnection();
$db = $database->getConnection();

$items = new User($db);

$stmt = $items->getUser();
$itemCount = $stmt->rowCount();

if ($itemCount > 0)
{
    $UserArr = array();
    $UserArr['body'] = array();
    $UserArr['itemCount'] = $itemCount;

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
        extract($row);
        $e = array(
            "id" => $id,
            "full_name" => $full_name,
            "email_id" => $email_id,
            "phone_no" => $phone_no,
            "gender" => $gender,
            "address" => $address,
            "city" => $city,
            "zip_code" => $zip_code,
            "country" => $country,
            "region" => $region,
            "created_at" => $created_at
        );

        array_push($UserArr['body'], $e);
    }
    echo json_encode($UserArr);
}
else
{
    http_response_code(404);
    echo json_encode(array(
        "message" => "No record found."
    ));
}

?>
