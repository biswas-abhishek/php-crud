# PHP CRUD

## Create, Read, Update And Delete

---

### Made With PHP, PostgreSQL And Host In Heroku

---

## Run App

### Setup **PHP** - https://www.php.net/

### Setup Server - [Apache](https://apache.org/)

### Setup Database - **_[PostgreSQL](https://www.postgresql.org/)_**

### Clone Git -

```bash
git clone git@gitlab.com:biswas-abhishek/php-crud.git
```

Website [Click Hare](https://php-crud-v1.herokuapp.com/)
