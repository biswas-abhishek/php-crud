<?php
// Create Database Connection Class
class DatabaseConnection
{
    // Connection Variable
    public $connection;

    // Connection Config Variable
    private $host = ""; // Host Name
    private $port = ""; // Port
    private $db_name = ""; // Database Name
    private $user = ""; // User Name
    private $password = ""; // Password

    public function getConnection()
    {
        $this->connection = null;

        // Try Database Connection
        try
        {
            $this->connection = new PDO("pgsql:host=$this->host;port=$this->port;dbname=$this->db_name", $this->user, $this->password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

        }
        catch(PDOExecption $error)
        {
            echo "Not Connected To Database " . $error->getMessage();
        }

        // Return Connection
        return $this->connection;
    }

}

?>
